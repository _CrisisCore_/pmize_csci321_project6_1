<?php
function get_products($product_id) {
    global $db;
    $query = 'SELECT * FROM products
              ORDER BY name';
    $statement = $db->prepare($query);
    $statement->execute();
    $products = $statement->fetchAll();
    $statement->closeCursor();
    return $products;
}
  
    function get_products_by_customer($email) {
       global $db;
       $query = 'SELECT * FROM products p
                 INNER JOIN registrations r
                 ON p.productCode = r.productCode 
                 INNER JOIN customers c
                 ON r.customerID = c.customerID
                 WHERE customers.email = :email';
       $statement = $db->prepare($query);
       $statement->bindValue(":email", $email);
       $statement->execute();
       $products = $statement->fetchAll();
       $statement->closeCursor();
       return $products;         
    
    }
     function get_product($product_code){
         global $db;
         $query = 'SELECT * FROM products
                  WHERE productCode = :product_code';
         $statement = $db->prepare($query);
       $statement->bindValue(":product_code", $product_code);
       $statement->execute();
       $products = $statement->fetch();
       $statement->closeCursor();
       return $product;       
     }
     function delete_product($product_code){
        global $db;
        $query = 'DELETE FROM products
              WHERE productCode = :product_code';
        $statement = $db->prepare($query);
        $statement->bindValue(':product_code', $product_code);
        $statement->execute();
        $statement->closeCursor();
     }
     function add_product($code, $name, $vers, $rel){
        global $db;
        $query = 'INSERT INTO products
                 (productCode, name, version, releaseDate)
                  VALUES
                 (:code, :name, :vers, :rel)';
        $statement = $db->prepare($query);
        $statement->bindValue(':vers', $vers);
        $statement->bindValue(':code', $code);
        $statement->bindValue(':name', $name);
        $statement->bindValue(':rel', $rel);
        $statement->execute();
        $statement->closeCursor();
     }
?>
