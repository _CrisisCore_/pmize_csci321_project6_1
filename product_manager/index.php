<?php
require('../model/database.php');
require('../model/product_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'show_product_list';
    }
}
if ($action == 'show_add_form') {
    $product_id = filter_input(INPUT_POST, 'product_id', 
            FILTER_VALIDATE_INT);
    $products = get_products($product_id);
    include('add_product.php');
}
else if ($action == 'add_product') {
    $product_id = filter_input(INPUT_POST, 'product_id', 
            FILTER_VALIDATE_INT);
    $code = filter_input(INPUT_POST, 'code');
    $name = filter_input(INPUT_POST, 'name');
    $vers = filter_input(INPUT_POST, 'vers');
    $rel = filter_input(INPUT_POST, 'rel');
    add_product($code, $name, $vers, $rel);
  
}
else if ($action == 'delete_product') {
     $product_code = filter_input(INPUT_POST, 'product_code', 
            FILTER_VALIDATE_INT);
     delete_product($product_code);
}
else if ($action == 'list_products')
{
      $email = filter_input(INPUT_POST, 'email', 
            FILTER_VALIDATE_INT);
      get_products_by_customer($email);
      include('product_list.php');
}
?>